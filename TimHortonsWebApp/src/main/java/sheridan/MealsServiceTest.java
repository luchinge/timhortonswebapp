package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	MealsService mealService = new MealsService();
	
	@Test //Success
	public void testDrinksRegular() {
		List<String> testMethod = mealService.getAvailableMealTypes(MealType.DRINKS);
		if(testMethod.isEmpty() == false) {
			for(int x = 0; x < testMethod.size(); x++) {
				System.out.println(testMethod.get(x));
			}
		}
		assertTrue("Meals Types is empty!", testMethod.isEmpty() == false);
	}

	@Test (expected = AssertionError.class) //Failure
	public void testDrinksException() {
		List<String> testMethod = mealService.getAvailableMealTypes(MealType.DRINKS);
		if(testMethod.isEmpty() == false) {
			for(int x = 0; x < testMethod.size(); x++) {
				System.out.println(testMethod.get(x));
			}
		}
		assertTrue("Meals Types is empty!", testMethod.isEmpty());		
	}
	
	@Test //BoundaryIn
	public void testDrinksBoundaryIn() {
		List<String> testMethod = mealService.getAvailableMealTypes(MealType.DRINKS);
		if(testMethod.size() <= 3) {
			System.out.println("MealType.DRINKS - LESS THAN 3.");
		}
		assertTrue("MealType.DRINKS - GREATER than 3.", testMethod.size() > 3);			
	}
	
	@Test //BoundaryIn
	public void testDrinksBoundaryOut() {
		List<String> testMethod = mealService.getAvailableMealTypes(null);
		if(testMethod.size() == 0) {
			System.out.println("MealType.DRINKS - Is NULL");
		}
		assertTrue("MealType.DRINKS - is NOT NULL", testMethod.size() != 0);			
	}	
	
}
